const getSum = (str1, str2) => {
  let int1 = getIntFromStr(str1);
  let int2 = getIntFromStr(str2);
  if(int1 === false || int2 === false)
    return false;
  return (int1 + int2).toString();
};
function getIntFromStr(value){
  if(typeof value !== "string")
    return false;
  if(value === '')
    return 0;
  for (const string of value.split('')) {
    if(isNaN(string))
      return false;
  }
  return parseInt(value);
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
 let result = { Post:0,comments:0};
  for (let post of listOfPosts) {
    if(post.author === authorName)
      result.Post++;
    if(post.comments !== undefined){
      for (let comment of post.comments) {
        if(comment.author === authorName)
          result.comments++;
      }
    }
  }
  let resultStr = "";
  for (const resultKey in result) {
    resultStr += resultKey + ":" + result[resultKey] + ","
  }
  return resultStr.substring(0,resultStr.length - 1);
};

const tickets=(people)=> {
  let money = 0;

  for (const personBill of people) {
    if(personBill === 25)
      money += personBill;
    else {
      if(money < personBill - 25)
        return "NO";
      money = money - 25 + personBill;
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
